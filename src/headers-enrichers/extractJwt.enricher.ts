export const jwtFromRequest = (ctx, rh) => {
    if (ctx.request && ctx.request.headers && ctx.request.headers['authorization']) {
        rh['Authorization'] = ctx.request.headers['authorization'];
    }

    return rh;
}