import * as crypto from 'crypto';

export const traceIdFromRequest = (ctx, headers) => {
    if (ctx.request && ctx.request.headers[ctx.traceIDHeader]) {
        headers[ctx.traceIDHeader] = ctx.request.headers[ctx.traceIDHeader] as string;
    } else if(ctx.request?.res && ctx.request.res['locals']?.traceID) {
        headers[ctx.traceIDHeader] = ctx.request.res['locals'].traceID;
    } else {
        headers[ctx.traceIDHeader] = crypto.randomUUID({ disableEntropyCache: true });
    }
    return headers;
}
