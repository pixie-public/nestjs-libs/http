import type { status as GrpcStatusCode } from '@grpc/grpc-js';
import { getGrpcFromHttp } from "./http-codes-map";

export type GrpcExceptionPayload = {
  message: string;
  code: GrpcStatusCode | number;
};

/**
 * generates the grpc error object
 * @param {string} exceptionName - Exception class name of the exception
 * @param {string | object} error - The error message or payload
 * @param {number} httpCode - The http error code to be converted to GrpcStatusCode ("import type { status } from "@grpc/grpc-js")
 * @returns {GrpcExceptionPayload}
 */
export function errorObject(
    exceptionName: string,
    error: string | object,
    httpCode: number,
): GrpcExceptionPayload {
  return <GrpcExceptionPayload>{
    message: JSON.stringify({
      code: getGrpcFromHttp(httpCode),
      error,
      type: exceptionName,
    }),
    code: getGrpcFromHttp(httpCode),
  };
}
