import { HttpStatus } from '@nestjs/common';
import { Status } from '@grpc/grpc-js/build/src/constants';

const grpc = 'grpc';
const http = 'http';
export const HTTP_CODE_FROM_GRPC: Record<string, number> = {
  //grpc
  [`${grpc}_${Status.OK}`]: HttpStatus.OK,
  [`${grpc}_${Status.CANCELLED}`]: HttpStatus.METHOD_NOT_ALLOWED,
  [`${grpc}_${Status.UNKNOWN}`]: HttpStatus.BAD_GATEWAY,
  [`${grpc}_${Status.INVALID_ARGUMENT}`]: HttpStatus.UNPROCESSABLE_ENTITY,
  [`${grpc}_${Status.DEADLINE_EXCEEDED}`]: HttpStatus.REQUEST_TIMEOUT,
  [`${grpc}_${Status.NOT_FOUND}`]: HttpStatus.NOT_FOUND,
  [`${grpc}_${Status.ALREADY_EXISTS}`]: HttpStatus.CONFLICT,
  [`${grpc}_${Status.PERMISSION_DENIED}`]: HttpStatus.FORBIDDEN,
  [`${grpc}_${Status.RESOURCE_EXHAUSTED}`]: HttpStatus.TOO_MANY_REQUESTS,
  [`${grpc}_${Status.FAILED_PRECONDITION}`]: HttpStatus.PRECONDITION_REQUIRED,
  [`${grpc}_${Status.ABORTED}`]: HttpStatus.METHOD_NOT_ALLOWED,
  [`${grpc}_${Status.OUT_OF_RANGE}`]: HttpStatus.PAYLOAD_TOO_LARGE,
  [`${grpc}_${Status.UNIMPLEMENTED}`]: HttpStatus.NOT_IMPLEMENTED,
  [`${grpc}_${Status.INTERNAL}`]: HttpStatus.INTERNAL_SERVER_ERROR,
  [`${grpc}_${Status.UNAVAILABLE}`]: HttpStatus.NOT_FOUND,
  [`${grpc}_${Status.DATA_LOSS}`]: HttpStatus.INTERNAL_SERVER_ERROR,
  [`${grpc}_${Status.UNAUTHENTICATED}`]: HttpStatus.UNAUTHORIZED,

  //http
  [`${http}_${HttpStatus.OK}`]: Status.OK,
  [`${http}_${HttpStatus.METHOD_NOT_ALLOWED}`]: Status.CANCELLED,
  [`${http}_${HttpStatus.BAD_GATEWAY}`]: Status.UNKNOWN,
  [`${http}_${HttpStatus.UNPROCESSABLE_ENTITY}`]: Status.INVALID_ARGUMENT,
  [`${http}_${HttpStatus.REQUEST_TIMEOUT}`]: Status.DEADLINE_EXCEEDED,
  [`${http}_${HttpStatus.NOT_FOUND}`]: Status.NOT_FOUND,
  [`${http}_${HttpStatus.CONFLICT}`]: Status.ALREADY_EXISTS,
  [`${http}_${HttpStatus.FORBIDDEN}`]: Status.PERMISSION_DENIED,
  [`${http}_${HttpStatus.TOO_MANY_REQUESTS}`]: Status.RESOURCE_EXHAUSTED,
  [`${http}_${HttpStatus.PRECONDITION_REQUIRED}`]: Status.FAILED_PRECONDITION,
  [`${http}_${HttpStatus.METHOD_NOT_ALLOWED}`]: Status.ABORTED,
  [`${http}_${HttpStatus.PAYLOAD_TOO_LARGE}`]: Status.OUT_OF_RANGE,
  [`${http}_${HttpStatus.NOT_IMPLEMENTED}`]: Status.UNIMPLEMENTED,
  [`${http}_${HttpStatus.INTERNAL_SERVER_ERROR}`]: Status.INTERNAL,
  [`${http}_${HttpStatus.NOT_FOUND}`]: Status.UNAVAILABLE,
  [`${http}_${HttpStatus.INTERNAL_SERVER_ERROR}`]: Status.DATA_LOSS,
  [`${http}_${HttpStatus.UNAUTHORIZED}`]: Status.UNAUTHENTICATED,
};

export function getGrpcFromHttp(status: number) {
  return HTTP_CODE_FROM_GRPC[`${http}_${status}`];
}

export function getHttpFromGrpc(status: number) {
  return HTTP_CODE_FROM_GRPC[`${grpc}_${status}`];
}
