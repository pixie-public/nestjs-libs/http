import { ExecutionContext } from "@nestjs/common";
import { HttpArgumentsHost, RpcArgumentsHost } from "@nestjs/common/interfaces/features/arguments-host.interface";
import { Logger } from "@pixie-sh/logger";
import { getClientIp } from "@supercharge/request-ip";
import {IncomingMessage} from "http";

export const TrueClientIp = 'true-client-ip'; // from k8s lb
export const CfConnectingIp = 'cf-connecting-ip'; // from cloudflare lb
export const XOriginalForwardedFor = 'x-original-forwarded-for'; // most used one
export const XForwardedFor = 'x-forwarded-for'; // usually with proxy ip - fallback

export function isRequestRpc(ctx: ExecutionContext): boolean {
    return ctx.getType() === 'rpc';
}

export function isRequestHttp(ctx: ExecutionContext | IncomingMessage): boolean {
    return ctx instanceof IncomingMessage ? true : ctx.getType() === 'http';
}

export function getHttpContext(ctx: ExecutionContext | IncomingMessage): HttpArgumentsHost {
    return  ctx instanceof IncomingMessage ? ((ctx as any).res) : ctx?.switchToHttp();
}

export function getRpcContext(ctx: ExecutionContext): RpcArgumentsHost {
    return ctx?.switchToRpc();
}

export function getIP(ctx: ExecutionContext | IncomingMessage): RpcArgumentsHost {
    const isHttp = isRequestHttp(ctx);

    if (isHttp) {
        const request = getHttpContext(ctx).getRequest();

        const realIp =
            request.headers[XOriginalForwardedFor] ||
            request.headers[TrueClientIp] ||
            request.headers[CfConnectingIp] ||
            request.headers[XForwardedFor] ||
            getClientIp(request); //third party fallback

        return realIp;
    }

    return (ctx as ExecutionContext)?.getArgByIndex(2)?.getPeer();
}

export function getLocals(ctx: ExecutionContext | IncomingMessage) {
    const isHttp = isRequestHttp(ctx);

    return isHttp
        ? ((getHttpContext(ctx) as any)?.locals || (getHttpContext(ctx) as any)?.getResponse()?.locals)
        : (getRpcContext(ctx as ExecutionContext) as any)?.getContext()?.locals;
}

export function getLogger(ctx: ExecutionContext | IncomingMessage) {
    return getLocals(ctx)?.logger || Logger;
}