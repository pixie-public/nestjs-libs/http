import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Logger } from "@pixie-sh/logger";
import {getLocals, PixieRequest} from "../request";

export const GetLocals = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
        return getLocals(ctx);
    },
);
