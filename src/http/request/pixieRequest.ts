import { ExecutionContext, Injectable } from '@nestjs/common';
import { Request, Response } from 'express';
import { Metadata } from '@grpc/grpc-js';
import {
  HttpArgumentsHost,
  RpcArgumentsHost,
} from '@nestjs/common/interfaces/features/arguments-host.interface';
import { PixieLocals, PixieMetadata } from './pixieMetadata';
import { ServerUnaryCall } from '@grpc/grpc-js/src/server-call';
import { getHttpContext, getRpcContext, isRequestHttp, isRequestRpc } from "./helpers";

@Injectable()
export class PixieRequest {
  constructor(private context: ExecutionContext) {
  }

  get isHttp() {
    return isRequestHttp(this.context);
  }

  get isGrpc() {
    return isRequestRpc(this.context);
  }

  get http(): HttpArgumentsHost {
    return getHttpContext(this.context);
  }

  get grpc(): RpcArgumentsHost {
    return getRpcContext(this.context);
  }

  getGrpcCall(): ServerUnaryCall<any, any> {
    return this.context?.getArgByIndex(2);
  }

  getNextHandler() {
    return this.isHttp
      ? (this.getHttpRequest() as any)?.route?.path
      : this.context?.getHandler()?.name;
  }

  getTraceId() {
    return this.getLocals().traceID;
  }

  hasHeader(headerKey: string) {
    return !!this.getHeader(headerKey);
  }

  getHttpResponse(): Response {
    return this.http?.getResponse();
  }

  getHttpRequest(): Request {
    return this.http?.getRequest();
  }

  getOriginalGrpcMetadata(): Metadata {
    return this.grpc?.getContext();
  }

  getGrpcData(): any {
    return this.grpc?.getData();
  }

  getGrpcMetadata(): PixieMetadata<PixieLocals> {
    const meta: any = this.getOriginalGrpcMetadata();
    if (!meta.locals) {
      meta.locals = {};
    }
    return meta;
  }

  getHeader(headerKey: string) {
    if (this.isHttp) {
      const req: any = this.getHttpRequest();
      return req?.headers && req?.headers[headerKey];
    } else if (this.isGrpc) {
      return this.getGrpcMetadata().get(headerKey);
    }

    return null;
  }

  getHeaders() {
    if (this.isHttp) {
      const req: any = this.getHttpRequest();
      return req?.headers;
    } else if (this.isGrpc) {
      return this.getGrpcMetadata();
    }

    return null;
  }

  getLogger() {
    return this.getLocals()?.logger;
  }

  getLocals() {
    return this.isHttp
      ? (this.getHttpResponse() as any)?.locals
      : this.getGrpcMetadata()?.locals;
  }

  addToLocals(key, value) {
    if (this.isHttp) {
      this.getLocals()[key] = value;
    } else if (this.isGrpc) {
      this.getLocals()[key] = value;
    }
  }
}