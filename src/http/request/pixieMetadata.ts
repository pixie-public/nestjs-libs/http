import { Metadata } from '@grpc/grpc-js';
import { JsonLogger } from "@pixie-sh/logger";

export interface PixieMetadata<T extends PixieLocals> extends Metadata {
  locals: T;
}

export interface PixieLocals {
  logger: JsonLogger;
  traceID: string;
  jwt: string;
  jwt_validity: number;
}
