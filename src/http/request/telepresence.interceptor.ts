import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AxiosRequestConfig } from 'axios';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class TelepresenceInterceptor implements NestInterceptor {
    private allowTelepresence: boolean;

    constructor(private httpService: HttpService, protected configService: ConfigService) {
        this.allowTelepresence = this.configService.get<boolean>('allowTelepresence', false);
    }

    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        if (this.allowTelepresence) {
            const ctx = context.switchToHttp();
            const request = ctx.getRequest();

            // Extract the x-telepresence-id header
            const telepresenceId = request.headers['x-telepresence-id'];

            if (telepresenceId) {
                // Add a request interceptor to HttpService
                this.httpService.axiosRef.interceptors.request.use((config: AxiosRequestConfig) => {
                    config.headers['x-telepresence-id'] = telepresenceId;
                    return config;
                });
            }
        }

        return next.handle();
    }
}
