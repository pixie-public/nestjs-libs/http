import { Request, Response } from 'express';
import { Logger, LoggerInterface } from "@pixie-sh/logger";
import { ExecutionContext } from "@nestjs/common";
import { PixieRequest } from "../request";

export const collectInitialMetrics = function (
    request: PixieRequest,
    now: number,
) {
  const log = request
      .getLogger()
      .with('path', request.getNextHandler())
      .with('startTime', now);

  if (process.env.DEBUG_LEVEL === 'TRUE') {
    log.with('request', JSON.stringify(dumpRequest(request)));
  }

  return log;
};

export const collectFinalMetrics = function (
    innerLog,
    startTime,
    data,
    request,
) {
  const end = new Date().getTime();
  innerLog.with('endTime', end);
  innerLog.with('executionDelta', end - startTime);
  if (request.isHttp) {
    innerLog.with('httpCode', request.getHttpResponse().statusCode);
  }

  if (process.env.DEBUG_LEVEL === 'TRUE') {
    innerLog.with('response', JSON.stringify(dumpResponse(request, data)));
  }

  return innerLog;
};

export const dumpRequest = function (req: Request | PixieRequest) {
  if (req instanceof PixieRequest && req.isGrpc) {
    const sreq: PixieRequest = req;
    return {
      url: sreq.getGrpcCall().getPath(),
      rawHeaders: sreq.getOriginalGrpcMetadata(),
      body: sreq.getGrpcData(),
      ip: sreq.getGrpcCall().getPeer(),
    };
  } else {
    let sreq;
    if (req instanceof PixieRequest) {
      sreq = (req as PixieRequest).getHttpRequest();
    } else {
      sreq = req;
    }
    const body = sreq.body;
    const { rawHeaders, httpVersion, method, socket, url } = sreq;
    const { remoteAddress, remoteFamily } = socket;

    return {
      url,
      rawHeaders,
      body,
      httpVersion,
      method,
      remoteAddress,
      remoteFamily,
    };
  }
};

export const dumpResponse = function (
    response: Response | PixieRequest,
    body: any = {},
) {
  if (response instanceof PixieRequest && response.isGrpc) {
    const sreq: PixieRequest = response;
    return {
      headers: sreq.getOriginalGrpcMetadata(),
      body,
      locals: sreq.getLocals(),
    };
  } else {
    let sresponse;
    if (response instanceof PixieRequest) {
      sresponse = (response as PixieRequest).getHttpResponse();
    } else {
      sresponse = response;
    }
    const locals = sresponse.locals;
    const { statusCode, statusMessage } = sresponse;
    const headers = sresponse.getHeaders();

    return {
      statusCode,
      statusMessage,
      headers,
      body,
      locals,
    };
  }
};

export const getContextLogger = function (context: ExecutionContext): LoggerInterface {
  const request = new PixieRequest(context);
  return request.getLogger() || Logger;
};

export const getRequestLogger = function (request: Request): LoggerInterface {
  return request?.res?.locals?.logger || Logger;
};
