import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Logger } from "@pixie-sh/logger";
import { PixieRequest } from "../request";

export const GetLogger = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const req = new PixieRequest(ctx);
    return req.getLogger() || Logger;
  },
);
