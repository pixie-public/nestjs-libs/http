import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, tap } from 'rxjs';
import * as crypto from 'crypto';
import { collectFinalMetrics, collectInitialMetrics } from './functions';
import { JsonLogger, Logger } from "@pixie-sh/logger";
import { PixieRequest } from "../request";

@Injectable()
export class LoggerInterceptor implements NestInterceptor {
  static XInternalTraceID = 'x-internal-trace-id';

  constructor() {
    if (process.env.APP === undefined || process.env.SCOPE === undefined) {
      Logger.debug(
          'process.env.APP and process.env.SCOPE are undefined; default used;',
      );
    }
  }

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = new PixieRequest(context);

    if (
        request.getNextHandler() === '/health' ||
        request.getNextHandler() === 'health' ||
        request.getNextHandler() === 'serverReflectionInfo'
    ) {
      return next.handle();
    }

    if (request.hasHeader(LoggerInterceptor.XInternalTraceID)) {
      request.addToLocals(
          'traceID',
          request.getHeader(LoggerInterceptor.XInternalTraceID),
      );
    } else {
      request.addToLocals(
          'traceID',
          crypto.randomUUID({ disableEntropyCache: true }),
      );
    }

    request.addToLocals(
        'logger',
        new JsonLogger(process.env.APP, process.env.SCOPE, request.getTraceId()),
    );

    const start = new Date().getTime();
    const log = collectInitialMetrics(request, start);
    log.log(`start request ${request.getTraceId()}`);

    return next.handle().pipe(
        tap((data) => {
          collectFinalMetrics(log, start, data, request).log(
              `end request ${request.getTraceId()}`
          );
        }),
    );
  }
}
