export interface HttpErrorModel {
  status: number;
  code: string;
  message: string;
  callStack: string;
}
