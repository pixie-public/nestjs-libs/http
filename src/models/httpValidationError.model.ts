export interface HttpValidationErrorModel {
  message: string;
  field: string;
  rule: string;
}
