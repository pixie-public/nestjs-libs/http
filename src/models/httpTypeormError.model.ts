export interface HttpTypeormErrorModel {
  status: number;
  code: string;
  message: string;
  type: string;
  table: string;
}
