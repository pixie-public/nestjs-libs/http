import { HttpErrorModel } from "./httpError.model";

export interface HttpResponse<T> {
  data?: T;
  error?: HttpErrorModel;
}
