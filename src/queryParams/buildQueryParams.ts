export function buildQueryParams(data: any) {
  let query = '';
  for (const d in data) {
    if (query.length === 0) {
      query += '?';
    } else {
      query += '&';
    }

    query += d + '=' + data[d];
  }
  return query;
}
