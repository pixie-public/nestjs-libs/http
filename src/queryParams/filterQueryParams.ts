export interface FilterQueryParams {
  page?: number;
  limit?: number;
  ids?: string;
  search?: string;
  order_by?: string;
  per_page?: string;
}
