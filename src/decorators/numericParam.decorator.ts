import { Param, ParseIntPipe } from '@nestjs/common';

export const NumericParamDecorator = (data?: any, ...pipes: []) => {
  return Param(data, new ParseIntPipe(), ...pipes);
};
