import { Query, ParseIntPipe } from '@nestjs/common';

export const NumericQueryDecorator = (data?: any, ...pipes: []) => {
  return Query(data, new ParseIntPipe(), ...pipes);
};
