import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { getIP } from "../http";

const _proxiedIP = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
       return getIP(ctx);
    },
);

export const ProxiedIp = _proxiedIP;
