import { Inject, Injectable, Optional } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { REQUEST } from '@nestjs/core';
import {
  RestClientService,
  RestClientServiceOptions,
  RestHeaders,
} from '../api-service';
import { Request } from 'express';
import {jwtFromRequest} from "../headers-enrichers/extractJwt.enricher";
import {traceIdFromRequest} from "../headers-enrichers/traceId.enricher";

@Injectable()
export class AuthenticatedRestClientService extends RestClientService {
  static XInternalTraceID = 'x-internal-trace-id'

  private readonly traceIDHeader: string;

  constructor(
    options: RestClientServiceOptions,
    configService: ConfigService,
    httpService: HttpService,
    @Optional() @Inject(REQUEST) private request: Request = null,
  ) {
    super(options, configService, httpService);

    this.options.headersEnrichers.push(jwtFromRequest)
    this.options.headersEnrichers.push(traceIdFromRequest)

    this.traceIDHeader = this.configService.get<string>(`${this.options._prefix}.xTraceIdHeader`, AuthenticatedRestClientService.XInternalTraceID);
  }

  protected enrichHeaders(ctx: any, headers: RestHeaders): RestHeaders {
    const h = super.enrichHeaders({
      ...ctx,
      traceIDHeader: this.traceIDHeader,
      request: this.request,
    }, headers);
    return h
  }

}
