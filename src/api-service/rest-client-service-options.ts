import { Injectable } from '@nestjs/common';
import { RestHeaders } from "./types";

@Injectable()
export class RestClientServiceOptions {
  constructor(public apiKeysPerHost, public _prefix: string = 'rest', public headersEnrichers: ((ctx: any, headers: RestHeaders) => RestHeaders)[] = []) {}
}
