import { Observable } from 'rxjs';
import {AxiosRequestConfig, AxiosRequestHeaders, AxiosResponse} from 'axios';

export type RestHeaders = AxiosRequestHeaders;
export type RestResponse<RES> = Observable<AxiosResponse<RES>>;

export interface RequestOptions<REQ> extends AxiosRequestConfig{
  headers?: RestHeaders;
  payload?: REQ;
}

export interface ResponseOptions<RES> {
  asResponseObject?: boolean;
}

export const HeaderApiKey = 'ApiKey';
