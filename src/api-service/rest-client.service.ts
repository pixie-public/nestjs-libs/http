import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { Method } from 'axios';
import { map, Observable } from 'rxjs';
import { ConfigService } from '@nestjs/config';
import { RestClientServiceOptions } from './rest-client-service-options';
import {
  HeaderApiKey,
  RequestOptions,
  ResponseOptions,
  RestHeaders,
  RestResponse,
} from './types';
import { Logger } from "@pixie-sh/logger";

@Injectable()
export class RestClientService {
  constructor(
      protected options: RestClientServiceOptions,
      protected configService: ConfigService,
      protected httpService: HttpService) {

    this.options.headersEnrichers.push((ctx, rh) => {
      const apiKey = this.options.apiKeysPerHost[ctx.host];
      if (apiKey) rh[HeaderApiKey] = apiKey;
      return rh;
    });
  }

  get<RES, REQ>(
    url: string,
    requestOptions?: RequestOptions<REQ>,
    responseOptions?: ResponseOptions<RES>,
  ): RestResponse<RES> | Observable<RES> {
    return this.execute<RES, REQ>('get', url, requestOptions, responseOptions);
  }

  post<RES, REQ>(
    url: string,
    requestOptions?: RequestOptions<REQ>,
    responseOptions?: ResponseOptions<RES>,
  ): RestResponse<RES> | Observable<RES> {
    return this.execute<RES, REQ>('post', url, requestOptions, responseOptions);
  }

  put<RES, REQ>(
    url: string,
    requestOptions?: RequestOptions<REQ>,
    responseOptions?: ResponseOptions<RES>,
  ): RestResponse<RES> | Observable<RES> {
    return this.execute<RES, REQ>('put', url, requestOptions, responseOptions);
  }

  delete<RES, REQ>(
    url: string,
    requestOptions?: RequestOptions<REQ>,
    responseOptions?: ResponseOptions<RES>,
  ): RestResponse<RES> | Observable<RES> {
    return this.execute<RES, REQ>(
      'delete',
      url,
      requestOptions,
      responseOptions,
    );
  }

  execute<RES, REQ>(
    method: string,
    url: string,
    options?: RequestOptions<REQ>,
    responseOptions?: ResponseOptions<RES>,
  ): RestResponse<RES> | Observable<RES> {
    const urlObj = new URL(url); //TODO proper handle url errors

    if (!options) {
      options = {};
    }

    if (!responseOptions) {
      responseOptions = {};
    }

    const req = {
      ...options,
      headers: this.enrichHeaders({ host: urlObj.host }, options.headers),
      url: url,
      method: method as Method,
      data: options.payload,
    };

    Logger.debug('RestClientService executing {0} request to {1} with: {2}', method, url, req);
    const response = this.httpService.request(req);

    //response object?
    if (responseOptions.asResponseObject) {
      return response as any;
    }

    //nup, return data only
    return response.pipe(map(res => res.data.data));
  }

  protected enrichHeaders(ctx: any, headers: RestHeaders): RestHeaders {
    if (!headers) {
      headers = {};
    }

    for (const fn of this.options.headersEnrichers) {
      headers = fn(ctx, headers);
    }

    return headers;
  }
}
